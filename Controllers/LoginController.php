<?php
namespace Controllers;

use \Core\Controller;
use \Models\Usuarios;


class LoginController extends Controller {

    public function index() {
        $dados = array(); 

        
        if(isset($_POST['usuario']) && !empty($_POST['usuario'])){  
           
            $usuario = addslashes($_POST['usuario']);
            $senha = $_POST['senha'];

            $u = new Usuarios();
            
            if($u->login($usuario, $senha)){
                header("location: ".BASE);
            }else{
                $dados['info'] = 'usuário e/ou senha incorreto';
            }                 
            
        }            
        
        $this->loadView('login', $dados);
    }
    public function logout(){
        $dados = array();
        $u = new Usuarios();

        $u->UsuarioDeslogado();
        
        unset($_SESSION['lguser']);
        header("Location: ".BASE);
        
    }
    public function registrar(){
        $dados = array();
        $this->loadView('register', $dados);
    }
    

}