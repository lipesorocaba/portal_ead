<?php
namespace Controllers;

use \Core\Controller;

class PortalController extends Controller {

	
	public function index() {
		$dados = array();		
		//header("Location:".BASE."login");	
		$this->loadTemplate2('portal/portal', $dados);
	}
	public function contato() {
		$dados = array();		
		//header("Location:".BASE."login");	
		$this->loadTemplate2('portal/contato', $dados);
	}
	public function sobre() {
		$dados = array();		
		//header("Location:".BASE."login");	
		$this->loadTemplate2('portal/sobre', $dados);
	}
	public function colaboradores() {
		$dados = array();		
		//header("Location:".BASE."login");	
		$this->loadTemplate2('portal/colaboradores', $dados);
	}
	
}