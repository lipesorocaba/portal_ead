<?php
namespace Core;

class Controller {

	public function loadView($viewName, $viewData = array()) {
		extract($viewData);
		require 'Views/'.$viewName.'.php';
	}

	public function loadTemplate($viewName, $viewData = array()) {
		require 'Views/template.php';
	}
	public function loadTemplate2($viewName, $viewData = array()) {
		require 'Views/template2.php';
	}

	public function loadViewInTemplate($viewName, $viewData = array()) {
		extract($viewData);
		require 'Views/'.$viewName.'.php';
	}
	public function loadlibrary($lib){
		if(file_exists('libraries/'.$lib.'.php')){
			include 'libraries/'.$lib.'.php';		
		}
	}

}