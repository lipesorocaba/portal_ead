
<!doctype html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" type="image/png" sizes="32x32" href="<?php echo BASE;?>assets/images/logo/coracao.png">

    <title>Somos Filhos</title>

    <link rel="canonical" href="https://getbootstrap.com/docs/4.0/examples/floating-labels/">

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="<?php echo BASE;?>assets/bootstrap/css/bootstrap.css">
    <link rel="stylesheet" href="<?php echo BASE;?>assets/css/custom.css">

     <!-- Font Awesome Icons -->
    <link rel="stylesheet" href="<?php echo BASE;?>assets/vendor/fontawesome-free/css/all.min.css">

    <!-- Custom styles for this template -->
    <link href="<?php echo BASE;?>assets/css/floating-labels.css" rel="stylesheet">
  </head>

  <body class="error-page">

  <div class="container ">
      
      <h4 class="display-1 text-center"><i class="fas fa-lock"></i></h4>
        <h5 class="lead text-center">seu perfil de usuário não pode acessar a página
          <a href="javascript:history.back()">voltar</a></h5>
          <h5 class="lead text-center"> ou retornar para página inicial
          <a href="<?php echo BASE;?>">Retornar ao sistema</a></h5>
  </div>
  <!-- Breadcrumbs-->
      

        <!-- Page Content -->
        

      </div>
      <!-- /.container-fluid -->
  </body>
</html>


 
