var data_calendario;
$(document).ready(function() {

  $('#agenda-geral').fullCalendar({

      
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    defaultView:'listWeek',
    minTime: "08:00:00",
    maxTime: "22:00:00",
    selectable: true,
     events:BASE+'agenda/getAgenda',
    
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    defaultDate: Date(),

 
    eventClick: function(obj){

      var cod = obj.id;
      $.ajax({
          url: BASE+'agenda/frm_consultar_agenda',
          type:'POST',
          data:{cod:cod},
          success:function(html){    
            $('#cadastro').find('.modal-body').html(html);
            $('#cadastro').modal('show');

      
          }
      });
      
    }
  });
  
});
var codigo_profissional;
var  nome_profissional;
$(document).ready(function() {

  $('#agenda-profissional').fullCalendar({

      
    editable: true,
    eventLimit: true, // allow "more" link when too many events
    defaultView:'agendaWeek',
    minTime: "08:00:00",
    maxTime: "22:00:00",
    selectable: true,
     events:BASE+'agenda/getAgendaByProfissional',
    
    header: {
      left: 'prev,next today',
      center: 'title',
      right: 'month,agendaWeek,agendaDay,listWeek'
    },
    defaultDate: Date(),

    dayClick: function(date) {
      var start = date.format();

      if(!nome_profissional){
        $('#cadastro').find('.modal-body').html("<p class='text-center'><strong>OPS! selecione um profissional</strong></p>");
        $('#cadastro').modal('show');
      }else{
        $.ajax({
            url: BASE+'agenda/frm_add_agenda',
            type:'POST',
            data:{
              start:start,
              cod_profissional:codigo_profissional,
              nome_profissional:nome_profissional
            },
            success:function(html){    
              $('#cadastro').find('.modal-body').html(html);
              $('#cadastro').modal('show');

              $('.cliente').hide("slow");
              $('.cadastrado').show("slow");

              $('#cadastrado').prop('required',true);
              $('#celular').prop('required',true);

              $('#cliente').prop('required',false);
              $('#celular_cliente').prop('required',false);
            }
        });
      }
    },
 
    select: function(startDate, endDate) {

      var start = startDate.format();
      var end = endDate.format();

      if(!nome_profissional){
        $('#cadastro').find('.modal-body').html("<p class='text-center'><strong>OPS! selecione um profissional</strong></p>");
        $('#cadastro').modal('show');
      }else{
        $.ajax({
          url: BASE+'agenda/frm_add_agenda',
          type:'POST',
          data:{
            start:start,
            end:end,
            cod_profissional:codigo_profissional,
            nome_profissional:nome_profissional
          },
          success:function(html){    
            $('#cadastro').find('.modal-body').html(html);
            $('#cadastro').modal('show');

            $('.cliente').hide("slow");
            $('.cadastrado').show("slow");

            $('#cadastrado').prop('required',true);
            $('#celular').prop('required',true);

            $('#cliente').prop('required',false);
            $('#celular_cliente').prop('required',false);
          }
        });
      }
      
    },

    eventClick: function(obj){

      var cod = obj.id;
      $.ajax({
          url: BASE+'agenda/frm_edit_agenda',
          type:'POST',
          data:{cod:cod},
          success:function(html){    
            $('#cadastro').find('.modal-body').html(html);
            $('#cadastro').modal('show');

      
          }
      });
      
    }



  });
  
});

function pesquisar_profissional_agenda(cod){
  var value = cod.value;
	var codigo = value.split('-');
  codigo_profissional = codigo[0];
  nome_profissional = codigo[1];
 
  
  document.getElementById("profissional-nome").innerHTML = codigo[1];

	$.ajax({
		url: BASE+'agenda/getAgendaByProfissional',
		type:'POST',
		data:{cod_profissional:codigo_profissional},
		success:function(){
      
      $('#agenda-profissional').fullCalendar('refetchEvents');
			
		}
  });
  
}
function add_agenda(){
	
	$.ajax({
		url: BASE+'agenda/add_agenda',
		type:'POST',
		success:function(html){

			$('#cadastro').find('.modal-body').html(html);
      $('#cadastro').modal('show');

      $('.cliente').hide("slow");
      $('.cadastrado').show("slow");
		}
	});

}

// function pesquisar_profissional_agenda(cod){
// 	var value = cod.value;
// 	var codigo = value.split('-');
//   cod_profissional = codigo[0];	
  
//   $.ajax({
// 		url: BASE+'agenda/get_agenda_profissional',
//     type:'POST',
//     data:{cod:cod_profissional},
// 		success:function(){

// 		}
// 	});
	
// }
var cod;
var cod_profissional;


function pesquisar_prof(cod){
	var value = cod.value;
	var codigo = value.split('-');
	cod_profissional = codigo[0];	
	
}
function pesquisas(data,codigo){

  var data_agenda = data;
  var cod_prof = codigo;

  $.ajax({
		url: BASE+'ajax/pesquisar_profissional',
		type:'POST',
		data:{data_agenda:data_agenda,cod_prof:cod_prof},
		dataType:'json',
		success:function(json){
			var html = '';
			
			for(var i in json){
			html +='<option value="'+json[i]+'">'+json[i]+'</option>';
			}
			$("#horario_inicial").html(html);
			
		}
	});
	$.ajax({
		url: BASE+'ajax/pesquisar_horario',
		type:'POST',
		data:{data_agenda:data_agenda,cod_prof:cod_prof},
		dataType:'json',
		success:function(json){
			var html = '';
			
			for(var i in json){
			html +='<option value="'+json[i]+'">'+json[i]+'</option>';
			}
			$("#horario_final").html(html);
			
		}
	});

}
function data_agenda(){
	
	var data_agenda = document.getElementById("dt_agenda").value;
  var cod_prof = cod_profissional;
  
  pesquisas(data_agenda,cod_prof);
	
	
}
function deletar_agenda(cod){
    $.ajax({
    url: BASE+'agenda/frm_delete_agenda',
    type:'POST',
    data:{cod:cod},
    success:function(html){

      $('#cadastro').find('.modal-body').html(html);
      $('#cadastro').modal('show');
    }
  });

}
