limite = 1800;
segundos = 0;
function contador(){  
	
	segundos = segundos + 1;
	
	if (segundos == limite) {
		
		$.ajax({
			url: BASE+'login/logout',
			type:'POST',
			success:function(html){
				
			}
		});
		
		Swal.fire({
			title: 'sessão expirou!',
			text: 'será necessário fazer login novamente!',
			type: 'error',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Login'
		}).then((result) => {
			if (result.value) {
				location.reload();
			}
		})
		
		
	}   
}

function timer(){
	setInterval(contador,1000);
	contador();
};
$(function(){	
	timer();
});




// adicionar foto do usuario

function frm_adicionar_foto(){
	
	$.ajax({
		url: BASE+'configuracao/frm_adicionar_foto',
		type:'POST',
		success:function(html){
			
			$('#modal-default').find('.modal-body').html(html);
			$('#modal-default').modal('show');				
		}
	});
	
}
function frm_adicionar_foto_membro(cod){
	
	$.ajax({
		url: BASE+'secretaria/frm_adicionar_foto_membro',
		type:'POST',
		data:{cod:cod},
		success:function(html){
			
			$('#modal-default').find('.modal-body').html(html);
			$('#modal-default').modal('show');				
		}
	});
	
}

$(function(){
	$('#form').on('submit', function(e){
		e.preventDefault();
		
		var cod_usuario = $("input[type=hidden][name=cod_usuario").val();
		var nome = $("input[type=text][name=nome").val();
		var sobrenome = $("input[type=text][name=sobrenome").val();
		var senha = $("input[type=password][name=senha").val();
		var confirma_senha = $("input[type=password][name=confirma_senha").val();
		
		if(senha != confirma_senha){
			
			const Toast = Swal.mixin({
				toast: true,
				position: 'top-end',
				showConfirmButton: false,
				timer: 5000
				
			});  
			toastr.error('OPS! as senhas não são idênticas, tente novamente!')
			
			$("input[type=password][name=senha").val(' ');
			$("input[type=password][name=confirma_senha").val(' ');
			$("input[type=password][name=senha").removeClass('is-valid');
			$("input[type=password][name=confirma_senha").removeClass('is-valid');
			return;
		}else{
			
			
			$.ajax({
				url: BASE+'configuracao/editar_acesso',
				type:'POST',
				data:{senha:senha,
					confirma_senha:confirma_senha,
					nome:nome,
					sobrenome:sobrenome,
					cod_usuario:cod_usuario},
					success:function(msg){
						
						Swal.fire({
							title: 'Salvo!',
							text: 'dados atualizado com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						
						
					}
				});
				
				
			}
			
			
		});
		
	});
	
function verificador_senha(){
	
	var senha = $("input[type=password][name=senha").val();
	var confirma_senha = $("input[type=password][name=confirma_senha]").val();
	
	var n = senha.length;
	if(n == 0){
		$("input[type=password][name=senha").removeClass('is-invalid');
		$("input[type=password][name=senha").removeClass('is-valid');
	}else if(n>0 && n < 6){
		$("input[type=password][name=senha").removeClass('is-valid');
		$("input[type=password][name=senha").addClass('is-invalid');
	}else if(n >= 6){
		$("input[type=password][name=senha").removeClass('is-invalid');
		$("input[type=password][name=senha").addClass('is-valid');
	}
	
	var m = confirma_senha.length;
	if(m == 0){
		$("input[type=password][name=confirma_senha").removeClass('is-invalid');
		$("input[type=password][name=confirma_senha").removeClass('is-valid');
	}else if(m > 0 && m < 6){
		$("input[type=password][name=confirma_senha").removeClass('is-valid');
		$("input[type=password][name=confirma_senha").addClass('is-invalid');
	}else if(m >= 6){
		$("input[type=password][name=confirma_senha").removeClass('is-invalid');
		$("input[type=password][name=confirma_senha").addClass('is-valid');
	}
	
};

function frm_add_usuario(){	
	
	$.ajax({
		url: BASE+'configuracao/frm_add_usuario',
		type:'POST',
		success:function(html){
			
			$('#modal-default').find('.modal-body').html(html);
			$('#modal-default').modal('show');
		}
	});
}

function frm_edit_usuario(cod){
	$.ajax({
		url: BASE+'configuracao/frm_edit_usuario',
		type:'POST',
		data:{cod:cod},
		success:function(html){
			
			$('#modal-default').find('.modal-body').html(html);
			$('#modal-default').modal('show');
		}
	});
	
}

function delete_usuario(cod){
	Swal.fire({
		title: 'Deletar usuário!',
		text: 'isso não poderá ser desfeito!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'configuracao/deletar',
				type:'POST',
				data:{cod:cod},
				success:function(msg){
					
					Swal.fire({
						title: 'Deletado!',
						text: 'Usuário deletado com sucesso!',
						type: 'success',
						confirmButtonColor: '#3085d6',
						confirmButtonText: 'OK'
					}).then((result) => {
						if (result.value) {
							location.reload();
						}
					})
					
					
				}
			});
			
		}
	})
}

function delete_permissao(cod){
	
	
	Swal.fire({
		title: 'Deletar grupo de acesso!',
		text: 'só é possivel excluir se não possuir nenhum usuário no grupo!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'configuracao/permissoes/excluir',
				type:'POST',
				data:{cod:cod},
				success:function(msg){
					
					if(msg == false){
						
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('OPS! usuário vinculado ao grupo')
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'Grupo deletado com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								window.location.href = BASE+'configuracao/permissoes/resumo';
							}
						})
						
					}
					
				}
			});
			
		}
	})
}
//editar grupos de permissao
function edit_permissao(cod){
	$.ajax({
		url: BASE+'configuracao/edit_permissao',
		type:'POST',
		data:{cod:cod},
		success:function(html){
			
			$('#permissao').find('.modal-body').html(html);
			$('#permissao').modal('show');
		}
	});
	
}
function add_permissao(){
	$.ajax({
		url: BASE+'configuracao/add_permissao',
		type:'POST',
		success:function(html){
			
			$('#permissao').find('.modal-body').html(html);
			$('#permissao').modal('show');
		}
	});
	
}

function busca(){
	var nr_end = document.getElementById("nr_end").value;
	var endereco = document.getElementById('endereco').value;
	var cep = document.getElementById('cep').value;
	var bairro =document.getElementById('bairro').value;
	var cidade =document.getElementById('cidade').value;
	var uf =document.getElementById('uf').value;
	
	
	
	var key = 'pZ6FQiXX1tGJ2jFr9ycUZQcRt4yCAGPm';
	var API = "https://www.mapquestapi.com/geocoding/v1/address?key=";
	// var data = '&inFormat=json&outFormat=json&json={'+location+'},"options":{"thumbMaps":false,"maxResults":"1"}}';
	
	
	
	// var url_nova = API+key+data;
	
	$.ajax({
		url: API+key+'&json={"location":{"street":"'+nr_end+', '+endereco+', '+cep+', '+bairro+','+cidade+'"},"options":{"thumbMaps":false,"maxResults":"1"}}',
		type:'GET',
		dataType:'json',              
		success:function(json){
			
			document.getElementById('lat').value =  (json.results[0].locations[0].latLng.lat);
			document.getElementById('lng').value =  (json.results[0].locations[0].latLng.lng);
			
			
		}
	});
	
	
}

function pesquisarCPF(){
	var cpf = document.getElementById('cpf').value;
	
	var m = cpf.length;
	if(m == 0){
		$('#cpf').removeClass('is-invalid');
		$('#cpf').removeClass('is-valid');
		exit;
	}
	
	$.ajax({
		url: BASE+'secretaria/consultar_cpf',
		type:'POST',
		data:{cpf:cpf},
		success:function(msg){
			
			if(msg == true){
				
				$('#cpf').removeClass('is-valid');
				$('#cpf').addClass('is-invalid');
				
			}else{
				$('#cpf').removeClass('is-invalid');
				$('#cpf').addClass('is-valid');
				
			}
			
		}
		
	});
	
}
$(function(){
	$('#form-editarmembro').on('submit', function(e){
		e.preventDefault();
		var dados = $('#form-editarmembro').serialize();
		
		document.getElementById("nome-profile").innerHTML = document.getElementById('Nome').value;
		
		var data_corrigida = document.getElementById('dt_nasc').value;
		string = data_corrigida.split('-');
		
		data_corrigida = string[2]+'/'+string[1]+'/'+string[0];
		document.getElementById("dt_nasc-profile").innerHTML = data_corrigida;
		
		$.ajax({
			url: BASE+'secretaria/membresia/editar_dados_pessoais',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! dados pessoais atualizado com sucesso!')
					
				}
				
				
			}
		});
		
	});
});
$(function(){
	$('#form-editarendereco').on('submit', function(e){
		e.preventDefault();
		
		var dados = $('#form-editarendereco').serialize();
		
		$.ajax({
			url: BASE+'secretaria/membresia/editar_endereco',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! endereço atualizado com sucesso!')
					
				}
				
				
			}
		});
		
	});
});
$(function(){
	$('#form-editarministerial').on('submit', function(e){
		e.preventDefault();
		
		var dados = $('#form-editarministerial').serialize();
		
		$.ajax({
			url: BASE+'secretaria/membresia/editarministerial',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! dados ministerial atualizado com sucesso!')
					
				}
				
				
			}
		});
		
	});
});

$(function(){	
	mapa();
});

function mapa() {
	
	lat  = document.getElementById('lat').value;
	lng = document.getElementById('lng').value;
	Sexo = document.getElementById('Sexo').value;
	Nome = document.getElementById('Nome').value;
	
	if(Sexo == 'M'){
		var color = '#334CFF';
	}else if(Sexo == 'F'){
		var color = '#980101';
	}
	
	L.mapquest.key = 'pZ6FQiXX1tGJ2jFr9ycUZQcRt4yCAGPm';
	
	var map = L.mapquest.map('map', {
		center: [lat, lng],
		layers: L.mapquest.tileLayer('map'),
		zoom: 16
	});
	L.marker([lat, lng], {
		icon: L.mapquest.icons.marker(
			{
				primaryColor: color,
				secondaryColor: color,
				shadow: true,
				size: 'sm'
			}
			),
			draggable: false
		}).bindPopup(Nome).addTo(map);
		
		map.addControl(L.mapquest.control());
		
	}
function atualizar_mapa(){
	
	location.reload();
	mapa();	
}
$(function(){
	
	
	document.getElementById("nome-profile").innerHTML = document.getElementById('Nome').value;
	
	var data = document.getElementById('dt_nasc').value;
	string = data.split('-');
	
	data = string[2]+'/'+string[1]+'/'+string[0];
	
	document.getElementById("dt_nasc-profile").innerHTML = data;
});

function frm_adicionar_culto(){
	$.ajax({
		url: BASE+'secretaria/frm_adicionar_culto',
		type:'POST',
		success:function(html){
			
			$('#cultos').find('.modal-body').html(html);
			$('#cultos').modal('show');
		}
	});
	
}
function cultos_contador(){
	
	var adulto = document.getElementById('adulto').value;
	var crianca = document.getElementById('crianca').value;
	
	if(adulto.length == 0 ){
		adulto = 0;
	}
	if(crianca.length == 0 ){
		crianca = 0;
	}
	
	var total = parseInt(adulto) + parseInt(crianca);
	$("#total").val(total);
}
function delete_culto(cod){
	
	
	Swal.fire({
		title: 'Deletar culto cadastrado!',
		text: 'essa operação irá deletar o cadastro!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'secretaria/cultos/excluir',
				type:'POST',
				data:{cod:cod},
				success:function(msg){

					if(msg == 'blocked'){
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('possui apontamento, não pode ser excluído')
					}
					
					if(msg == false){
						
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('OPS! culto não pode ser excluído')
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'Cadastro deletado com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								window.location.href = BASE+'secretaria/cultos/resumo';
							}
						})
						
					}
					
				}
			});
			
		}
	})
}

// formulário discipulado
function delete_fase(cod){
	
	
	Swal.fire({
		title: 'Deletar fase cadastrada!',
		text: 'a fase será arquivada, o histórico será mantido!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'discipulado/fase/delete',
				type:'POST',
				data:{cod:cod},
				success:function(msg){
					
					if(msg == false){
						
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('OPS! fase não pode ser excluída')
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'fase arquivada com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								window.location.href = BASE+'discipulado/administracao';
							}
						})
						
					}
					
				}
			});
			
		}
	})
}
// atividade
function frm_adicionar_atividade(cod){
	$.ajax({
		url: BASE+'discipulado/frm_adicionar_atividade',
		type:'POST',
		data:{cod:cod},
		success:function(html){
			
			$('#discipulado').find('.modal-body').html(html);
			$('#discipulado').modal('show');
		}
	});
	
}
function frm_editar_atividade(cod){
	$.ajax({
		url: BASE+'discipulado/frm_editar_atividade',
		type:'POST',
		data:{cod:cod},
		success:function(html){
			
			$('#discipulado').find('.modal-body').html(html);
			$('#discipulado').modal('show');
		}
	});
	
}
function delete_atividade(cod){

	
	Swal.fire({
		title: 'Deletar atividade cadastrada!',
		text: 'a atividade será arquivada, o histórico será mantido!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'discipulado/atividades/delete',
				type:'POST',
				data:{cod:cod},
				success:function(msg){
					
					if(msg == false){
						
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('OPS! atividade não pode ser excluída')
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'atividade arquivada com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						
					}
					
				}
			});
			
		}
	})
}
// pesquisa dos integrantes
$(function() {
	$('#pesquisa_integrante').on('keyup',function(){
		var texto = $(this).val();
		var cod_grupo = $("input[type=hidden][name=cod_grupo]").val();
	
		$.ajax({
			url: BASE+'discipulado/ajax_membro',
			type:'POST',
			data:{texto:texto,cod_grupo:cod_grupo},
			success:function(html){
				$('#campo_formulario').html(html);
			}
		});
	
	});
	
})
// adicionar os integrantes
function adicionar_integrante(cod){
	
	var array = cod.split(",");
	var cod_grupo = array[1].trim();
	var Codigo = array[0].trim();

	
	$.ajax({
		url: BASE+'discipulado/ajax_adicionar_integrante',
		type:'POST',
		data:{Codigo:Codigo,cod_grupo:cod_grupo},
		success:function(html){
			
			$('.visualizar-integrantes').html(html);
			
		}
	});

}
// remover os integrantes
function remover_integrante(cod){
	
	var array = cod.split(",");
	var cod_grupo = array[1].trim();
	var Codigo = array[0].trim();

	Swal.fire({
		title: 'remover o integrante?',
		text: "você poderá adicionar o integrante novamente ",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'confirmar',
		cancelButtonText: 'cancelar'
	  }).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'discipulado/ajax_remover_integrante',
				type:'POST',
				data:{Codigo:Codigo,cod_grupo:cod_grupo},
				success:function(html){
					
					$('.visualizar-integrantes').html(html);
					
				}
			});
		}
	  })

	
	

}
$(function(){
	$('#form-editar-grupo').on('submit', function(e){
		e.preventDefault();
		
		var dados = $('#form-editar-grupo').serialize();
		
		$.ajax({
			url: BASE+'discipulado/grupos/editar',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! dados do grupo atualizado com sucesso!')
					
				}else{
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.error('OPS! não foi possivel atualizar os dados!')

				}
				
				
			}
		});
		
	});

	$('#form-editar-status').on('submit', function(e){
		e.preventDefault();

		var dados = $('#form-editar-status').serialize();

		Swal.fire({
			title: 'CANCELADO E FINALIZADO!',
			text: 'alteração do status,irá liberar todos os membros do grupo para ser adicionado em outros',
			type: 'warning',
			showCancelButton: true,
			cancelButtonText:'Cancelar',
			cancelButtonColor: '#d33',
			confirmButtonColor: '#3085d6',
			confirmButtonText: 'Confirmar'			

		}).then((result) => {
			if (result.value) {

				$.ajax({
					url: BASE+'discipulado/grupos/editar_status',
					type:'POST',
					data : dados,
					dataType: 'json',
					success:function(msg){
		
						if(msg == true){
							
							const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 5000
								
							});  
							toastr.success('SUCESSO! dados do grupo atualizado com sucesso!');
							window.location.href = BASE+'discipulado/grupos/resumo';
							
						}else{
							const Toast = Swal.mixin({
								toast: true,
								position: 'top-end',
								showConfirmButton: false,
								timer: 5000
								
							});  
							toastr.warning('OPS! a data final é inferior a data atual!')
		
						}
						
						
					}
				});


			}
		
		
		
		});
	});

})
$(function() {

	if ( $( "#campo_formulario" ).length ) { 
	
		var cod_grupo = $("input[type=hidden][name=cod_grupo]").val();
		
		$.ajax({
			url: BASE+'discipulado/lista_grupo',
			type:'POST',
			data:{cod_grupo:cod_grupo},
			dataType: 'json',
			success:function(json){
				var html = '';

				
				for(var i in json) {

					html += '<a class="float-right btn btn-outline-danger" role="button" id="btn'+ cont +'" onclick="remover_campo('+ cont +')"><i class="fas fa-minus"></i></a>';
					html += '<div class="row" id="campo'+ cont +'">';
					html += '<div class="form-group col-md-12">';
					
					html += '<input type="text" list="membro" name="integrante[]" class="form-control" value="'+json[i]['Codigo']+' - '+json[i]['Nome']+'" >';
					html += '<datalist id="membro">';

					for(var i in json) {
						
						html += '<option value="'+json[i]['Codigo']+' - '+json[i]['Nome']+'">';	

					}
					html += '</datalist>';
					
					html += '</div>';
					html += '</div>';

				}
				

				
				

			
				// html += '<a class="float-right btn btn-outline-danger" role="button" id="btn'+ cont +'" onclick="remover_campo('+ cont +')"><i class="fas fa-minus"></i></a>';
				// html += '<div class="row" id="campo'+ cont +'"><div class="form-group col-md-12"><input type="text" list="membro" name="integrante[]" class="form-control" required>';
				// html += '<datalist id="membro">';

				// for(var i in json) {
					
				// 	html += '<option value="'+json[i]['Codigo']+' - '+json[i]['Nome']+'">';	

				// }
				// html += '</datalist>';
				// html += '</div>';
				// html += '</div>';

				$( "#campo_formulario" ).append(html);
				
				
			}
		});
	}
})
$(function(){
	var cod_grupo = $("input[type=hidden][name=cod_grupo]").val();
	$.ajax({
		url: BASE+'discipulado/ajax_visualizar_trilho',
		type:'POST',
		data:{cod_grupo:cod_grupo},
		success:function(html){
			$('.visualizar-trilho').html(html);
		}

	});
})
function mudar_status(cod){
	var cod_grupo = $("input[type=hidden][name=cod_grupo]").val();
	var array = cod.split('/');
	var codigo_atividade = array[0];
	var status = array[1];
	var cod_atividadesBygrupo = array[2];
	
	
	$.ajax({
		url: BASE+'discipulado/ajax_atualizar_trilho',
		type:'POST',
		data:{
			cod_grupo:cod_grupo,
			codigo_atividade:codigo_atividade,
			status:status,
			cod_atividadesBygrupo:cod_atividadesBygrupo
		},
		success:function(html){
			$('.visualizar-trilho').html(html);
		}

	});

}
function habilitar_data() {
	var x = document.getElementById("status").value;
	
	if(x == '2' || x == '3' ){
		//$('form input[type="submit"]'). prop("disabled", true);
		$("input[type=date][name=dt_final]").prop("disabled", false);
		
	}
	if(x == '1' ){
		//$('form input[type="submit"]'). prop("disabled", true);
		$("input[type=date][name=dt_final]").prop("disabled", true);
		
	}
}
function delete_grupo(cod){

	
	Swal.fire({
		title: 'Deletar grupo!',
		text: 'essa ação é permanente!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'discipulado/grupos/delete',
				type:'POST',
				data:{cod:cod},
				success:function(msg){

					
					if(msg == false){
						
						window.location.href = BASE+'notfound/acesso';
						
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'grupo excluído com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						
					}
					
				}
			});
			
		}
	})
}
// carregar integrantes do grupo
$(function(){
	var cod_grupo = $("input[type=hidden][name=cod_grupo]").val();
	$.ajax({
		url: BASE+'discipulado/ajax_visualizar_integrantes',
		type:'POST',
		data:{cod_grupo:cod_grupo},
		success:function(html){
			$('.visualizar-integrantes').html(html);
		}

	});
})
///////////////////////////////////
///////////////////////////////////
// FINANCEIRO /////////////////////
$(function(){
		
	var data = document.getElementById('culto').value;

	var data = data.split('-');
	var data = data[2]+'/'+data[1]+'/'+data[0];

	document.getElementById("apontamento").innerHTML = data;

	document.getElementById("dia-name").innerHTML = document.getElementById('dia').value;
	
		
});
//editar culto
$(function(){
	$('#form-editarculto').on('submit', function(e){
		e.preventDefault();
		var dados = $('#form-editarculto').serialize();
		
		var data = document.getElementById('culto').value;
		var data = data.split('-');
		var data = data[2]+'/'+data[1]+'/'+data[0];

		document.getElementById("apontamento").innerHTML = data;
		document.getElementById("dia-name").innerHTML = $('#dia_Semana').find(":selected").text();;

					
		$.ajax({
			url: BASE+'financeiro/editar_culto',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! dados atualizado com sucesso!')
					notificar_edicao(dados);
				}
				
				
			}
		});
		
	});
});
// pesquisa dos membros
$(function() {
	$('#pesquisa_membro').on('keyup',function(){
		var texto = $(this).val();
		var cod_culto = $("input[type=hidden][name=cod_culto]").val();
	
		$.ajax({
			url: BASE+'financeiro/ajax_membro',
			type:'POST',
			data:{texto:texto,cod_culto:cod_culto},
			success:function(html){
				$('#contribuinte').html(html);
			}
		});
	
	});
	
})
// carregar contribuinte
$(function(){
	var cod_culto = $("input[type=hidden][name=cod_culto]").val();
	
	$.ajax({
		url: BASE+'financeiro/ajax_visualizar_contribuinte',
		type:'POST',
		data:{cod_culto:cod_culto},
		success:function(html){
			$('.visualizar-contribuinte').html(html);
			
		}

	});
	atualizar_contribuicao();
	
	
	
});
// adicionar os contribuinte
function adicionar_contribuinte(){
	var cod_culto = $("input[type=hidden][name=cod_culto]").val();
	var Codigo = $("input[type=hidden][name=Codigo]").val();
	var valor = $("input[type=text][name=valor]").val();
	

	if(valor.length == 0){
		const Toast = Swal.mixin({
			toast: true,
			position: 'top-end',
			showConfirmButton: false,
			timer: 5000
			
		});  
		toastr.error('OPS! é necessário preencher o campo de valor!')
		notificar_edicao(dados);
	}
	
	$.ajax({
		url: BASE+'financeiro/ajax_adicionar_contribuinte',
		type:'POST',
		data:{Codigo:Codigo,cod_culto:cod_culto,valor:valor},
		success:function(html){
			
			$('.visualizar-contribuinte').html(html);
			$('#pesquisa_membro').val("");
			$('#contribuinte').html("");
			atualizar_contribuicao();
		}
	});
	

}
// remover os contribuinte
function remover_contribuinte(cod){
	
	var cod_culto = $("input[type=hidden][name=cod_culto]").val();
	var Codigo = $("input[type=hidden][name=Codigo]").val();
	var valor = $("input[type=text][name=valor]").val();

	Swal.fire({
		title: 'remover contribuição?',
		text: "você poderá adicionar novamente ",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'confirmar',
		cancelButtonText: 'cancelar'
	  }).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'financeiro/ajax_remover_contribuinte',
				type:'POST',
				data:{Codigo:Codigo,cod_culto:cod_culto,valor:valor},
				success:function(html){
					
					$('.visualizar-contribuinte').html(html);
					$('#pesquisa_membro').val("");
					$('#contribuinte').html("");
					atualizar_contribuicao();
					
				}
			});
		}
	  })

	
	

}
function salvar_oferta(cod){
	var cod_culto = cod;
	var valor = document.getElementById('oferta').value;

	$.ajax({
		url: BASE+'financeiro/ajax_adicionar_oferta',
		type:'POST',
		data:{cod_culto:cod_culto,valor:valor},
		dataType:'json',
		success:function(msg){
			
			if(msg == true){
					
				const Toast = Swal.mixin({
					toast: true,
					position: 'top-end',
					showConfirmButton: false,
					timer: 5000
					
				});  
				toastr.success('Oferta foi salva com sucesso!')
				
			};
			atualizar_contribuicao();
			
			
		}
	});
}
function atualizar_contribuicao(){
	var cod_culto = $("input[type=hidden][name=cod_culto]").val();
	$.ajax({
		url: BASE+'financeiro/atualizar_contribuicao',
		type:'POST',
		data:{cod_culto:cod_culto},
		dataType:'json',
		success:function(msg){
			var formato = { minimumFractionDigits: 2 , style: 'currency', currency: 'BRL' }
			var oferta = msg.oferta;
			var dizimo = msg.dizimo;
			var total = parseFloat(oferta) + parseFloat(dizimo);
						
			oferta_total = oferta.toLocaleString('pt-BR',formato);
			dizimo_total = dizimo.toLocaleString('pt-BR',formato);
			contribuicao_total = total.toLocaleString('pt-BR',formato);
			$('#oferta').val(oferta_total);
			$('#dizimo').val(dizimo_total);
			$('#contribuicao_total').val(contribuicao_total);
					
			
		}
	});

}
function delete_apontamento(cod){

	Swal.fire({
		title: 'Deletar Apontamento!',
		text: 'essa ação é permanente!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'financeiro/ajax_remover_apontamento',
				type:'POST',
				data:{cod:cod},
				dataType:'json',
				success:function(msg){
				
					if(msg =='blocked'){

						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.error('não possui permissão para excluir!')

					}
					
					if(msg == false){
						
						const Toast = Swal.mixin({
							toast: true,
							position: 'top-end',
							showConfirmButton: false,
							timer: 5000
							
						});  
						toastr.warning('não possui dados para excluir!')
						
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'apontamento excluído com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						
					}
					
				}
			});
			
		}
	})
	
}
//DESPESA MENSAL
$(function(){
	$.ajax({
        url: BASE+'financeiro/ajax_despesa_mensal',
        type:'POST',
        success:function(html){

            $('.despesa_mensal').html(html);
        }
    });
});
// botões para despesa mensal
function voltar_pagina_despesa(cod){
 
    $.ajax({
        url: BASE+'financeiro/ajax_despesa_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.despesa_mensal').html(html);
        }
    });
}
function avancar_pagina_despesa(cod){
   
    $.ajax({
        url: BASE+'financeiro/ajax_despesa_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.despesa_mensal').html(html);
        }
    });
}
function frm_apagar_despesas(cod){
	$.ajax({
		url: BASE+'financeiro/frm_apagar_despesas',
		type:'POST',
		data:{cod:cod},		
		success:function(html){

			$('#modal').find('.modal-body').html(html);
			$('#modal').modal('show');
		}
	
	});
}
//RELATORIO MENSAL
$(function(){
	$.ajax({
        url: BASE+'financeiro/ajax_relatorio_mensal',
        type:'POST',
        success:function(html){

            $('.relatorio_mensal').html(html);
        }
    });
});
// botões para resumo mensal
function voltar_pagina_resumo(cod){
 
    $.ajax({
        url: BASE+'financeiro/ajax_relatorio_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.relatorio_mensal').html(html);
        }
    });
}
function avancar_pagina_resumo(cod){
   
    $.ajax({
        url: BASE+'financeiro/ajax_relatorio_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.relatorio_mensal').html(html);
        }
    });
}
//DASHBOARD
$(function(){
	$.ajax({
        url: BASE+'financeiro/dashboard_ajax',
        type:'POST',
        success:function(html){

            $('.dashboard_financeiro').html(html);
        }
    });
});








$(function(){
	$.ajax({
        url: BASE+'financeiro/resumo_mensal_ajax',
        type:'POST',
        success:function(html){

            $('.receita_mensal').html(html);
        }
    });
})


function frm_add_receita(){
	$.ajax({
        url: BASE+'financeiro/frm_add_receita',
        type:'POST',
        success:function(html){

			$('#modal-receita').find('.modal-body').html(html);
			$('#modal-receita').modal('show');
		
        }
    });
}
function valor_categoria(){
	var valor = document.getElementById("categoria").value;

	if(valor === '1' ){
		$('#dizimista').show("slow");
		$('#dizimista').attr("required", true);
	}else{
		$('#dizimista').hide("slow");
		$('#visualizar-erro').hide('slow');
		$('#pesquisa_membro').val('');
		$('#dizimista').attr("required", false);
	}
}
function frm_editar_receita(cod){

	var codigo = cod;
	$.ajax({
        url: BASE+'financeiro/frm_editar_receita',
		type:'POST',
		data:{cod:codigo},
        success:function(html){

			$('#modal-receita').find('.modal-body').html(html);
			$('#modal-receita').modal('show');
		
        }
    });
}
function frm_deletar_receita(cod){

	
	Swal.fire({
		title: 'Deletar receita!',
		text: 'essa ação é permanente!',
		type: 'warning',
		confirmButtonColor: '#3085d6',
		confirmButtonText: 'Confirmar'
	}).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'financeiro/receitas/delete',
				type:'POST',
				data:{cod:cod},
				success:function(msg){

					
					if(msg == false){
						
						window.location.href = BASE+'notfound/acesso';
						
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'receita excluída com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						
					}
					
				}
			});
			
		}
	})
}
// botões para receita mensal
function voltar_pagina(cod){
 
    $.ajax({
        url: BASE+'financeiro/resumo_mensal_ajax',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.receita_mensal').html(html);
        }
    });
}
function avancar_pagina(cod){
   
    $.ajax({
        url: BASE+'financeiro/resumo_mensal_ajax',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.receita_mensal').html(html);
        }
    });
}

// editar despesa
function frm_editar_despesas(cod){	
	
    $.ajax({
        url: BASE+'financeiro/frm_editar_despesas',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('#modal').find('.modal-body').html(html);
            $('#modal').modal('show');


        }
    });
}
// pagamento da despesa
function pagar_despesa(cod){	
	
    $.ajax({
        url: BASE+'financeiro/frm_pagar_despesa',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('#modal').find('.modal-body').html(html);
            $('#modal').modal('show');


        }
    });
}
// deletar despesa
function deletar_despesa(cod){	
	
    $.ajax({
        url: BASE+'financeiro/frm_deletar_despesa',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('#modal').find('.modal-body').html(html);
            $('#modal').modal('show');


        }
    });
}

function voltar_mensal(cod){
 
    $.ajax({
        url: BASE+'financeiro/resumo_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.resumo_mensal').html(html);
        }
    });
}
function avancar_mensal(cod){
   
    $.ajax({
        url: BASE+'financeiro/resumo_mensal',
        type:'POST',
        data:{cod:cod},
        success:function(html){

            $('.resumo_mensal').html(html);
        }
    });
}
///////////////////////////////
///////////////////////////////
// TAREFAS
$(function(){
		
	document.getElementById("nome-tarefa").innerHTML = document.getElementById('tarefa').value;
	var data = document.getElementById('dt_final').value;
	

	  
	if(compareDates(data) === true){
		document.getElementById("status-tarefa").innerHTML = '<span class="badge badge-secondary float-right"> Estamos no prazo  </span>';
	}else{
		document.getElementById("status-tarefa").innerHTML = '<span class="badge badge-danger float-right"> tarefa está atrasada  </span>';
	}
		
});
function compareDates (date) {
	let parts = date.split('-') // separa a data pelo caracter '/'
	let today = new Date()      // pega a data atual
	
	date = new Date(parts[0], parts[1] - 1, parts[2]) // formata 'date'
	
	// compara se a data informada é maior que a data atual
	// e retorna true ou false
	return date > today ? true : false
}
$(function(){
	$('#form-editartarefa').on('submit', function(e){
		e.preventDefault();
		var dados = $('#form-editartarefa').serialize();
		
		document.getElementById("nome-tarefa").innerHTML = document.getElementById('tarefa').value;
		var data = document.getElementById('dt_final').value;	  
		if(compareDates(data) === true){
			document.getElementById("status-tarefa").innerHTML = '<span class="badge badge-secondary float-right"> Estamos no prazo  </span>';
		}else{
			document.getElementById("status-tarefa").innerHTML = '<span class="badge badge-danger float-right"> tarefa está atrasada  </span>';
		}
				
		$.ajax({
			url: BASE+'tarefas/editar',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! dados atualizado com sucesso!')
					notificar_edicao(dados);
				}
				
				
			}
		});
		
	});
});
$(function(){
	$('#form-editarcompartilhamento').on('submit', function(e){
		e.preventDefault();
		
		var dados = $('#form-editarcompartilhamento').serialize();
		
		$.ajax({
			url: BASE+'secretaria/membresia/editar_endereco',
			type:'POST',
			data : dados,
			dataType: 'json',
			success:function(msg){
				
				if(msg == true){
					
					const Toast = Swal.mixin({
						toast: true,
						position: 'top-end',
						showConfirmButton: false,
						timer: 5000
						
					});  
					toastr.success('SUCESSO! endereço atualizado com sucesso!')
					
				}
				
				
			}
		});
		
	});
});
// pesquisa dos usuarios na tarefa
$(function() {
	$('#pesquisa_usuarios').on('keyup',function(){
		var texto = $(this).val();
		var cod_tarefa = $("input[type=hidden][name=cod_tarefa]").val();
	
		$.ajax({
			url: BASE+'tarefas/ajax_usuario',
			type:'POST',
			data:{texto:texto,cod_tarefa:cod_tarefa},
			success:function(html){
				$('#lista_usuarios').html(html);
			}
		});
	
	});
	
})
// adicionar os integrantes
function adicionar_usuario_tarefa(cod){
	
	var array = cod.split(",");
	var cod_tarefa = array[1].trim();
	var cod_usuario = array[0].trim();

	$.ajax({
		url: BASE+'tarefas/ajax_adicionar_integrante',
		type:'POST',
		data:{cod_usuario:cod_usuario,cod_tarefa:cod_tarefa},
		success:function(html){
			
			$('.visualizar-compartilhamento').html(html);
			notificar_novo_integrante(cod);
		}
	});
	visualizar_integrantes_icone();
	

}
// remover os integrantes
function remover_usuario_tarefa(cod){
	
	var array = cod.split(",");
	var cod_tarefa = array[1].trim();
	var cod_usuario = array[0].trim();

	Swal.fire({
		title: 'remover o integrante?',
		text: "você poderá adicionar o integrante novamente ",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'confirmar',
		cancelButtonText: 'cancelar'
	  }).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'tarefas/ajax_remover_integrante',
				type:'POST',
				data:{cod_usuario:cod_usuario,cod_tarefa:cod_tarefa},
				success:function(html){
					
					$('.visualizar-compartilhamento').html(html);
					notificar_excluir_integrante(cod);
				}
			});
			visualizar_integrantes_icone();
		}
	  });

	  

	
	

}
// carregar integrantes do grupo
$(function(){
	var cod_tarefa = $("input[type=hidden][name=cod_tarefa]").val();
	
	$.ajax({
		url: BASE+'tarefas/ajax_visualizar_integrantes',
		type:'POST',
		data:{cod_tarefa:cod_tarefa},
		success:function(html){
			$('.visualizar-compartilhamento').html(html);
			
		}

	});
	visualizar_integrantes_icone();
	
	
});
function visualizar_integrantes_icone(){
	var cod_tarefa = $("input[type=hidden][name=cod_tarefa]").val();
	$.ajax({
		url: BASE+'tarefas/ajax_visualizar_integrantes_icone',
		type:'POST',
		data:{cod_tarefa:cod_tarefa},
		success:function(html){
			$('#lista-usuarios').html(html);
		}

	});

}
//atualizar o status da tarefa no menu
$(function() {

		$.ajax({
			url: BASE+'tarefas/ajax_notificacao',
			type:'POST',
			dataType: 'json',
			success:function(json){
				
				$('.tarefa-andamento').html(json.count_andamentos);
				$('.tarefa-nova').html(json.count_tarefas);
			}
		});
})

//excluir tarefas
function apagar_tarefa(cod){

	Swal.fire({
		title: 'deseja apagar a tarefa?',
		text: "você não poderá desfazer essa ação ",
		icon: 'warning',
		showCancelButton: true,
		confirmButtonColor: '#3085d6',
		cancelButtonColor: '#d33',
		confirmButtonText: 'confirmar',
		cancelButtonText: 'cancelar'
	  }).then((result) => {
		if (result.value) {
			$.ajax({
				url: BASE+'tarefas/ajax_apagar_tarefa',
				type:'POST',
				data:{cod_tarefa:cod},
				dataType: 'json',
				success:function(msg){

					console.log(msg);
		
					if(msg == false){
						
						const Toast = Swal.mixin({toast: true,position: 'top-end',showConfirmButton: false,timer: 5000});
						toastr.error('Seu usuário não tem permissão para excluir!')
						
						
					}else if(msg == true){
						
						Swal.fire({
							title: 'Deletado!',
							text: 'tarefa excluída com sucesso!',
							type: 'success',
							confirmButtonColor: '#3085d6',
							confirmButtonText: 'OK'
						}).then((result) => {
							if (result.value) {
								location.reload();
							}
						})
						notificar_exclusao();
						
					}
					
					
					
				}
			});
			
		}
	  });

}
function notificar_edicao(dados){
	
	$.ajax({
		url: BASE+'tarefas/notificar_edicao',
		type:'POST',
		data : dados,
		success:function(){
			
		}
	});
}
function notificar_novo_integrante(dados){

	var array = dados.split(",");
	var cod_tarefa = array[1].trim();
	var cod_usuario = array[0].trim();
	
	$.ajax({
		url: BASE+'tarefas/notificar_novo_integrante',
		type:'POST',
		data:{cod_usuario:cod_usuario,cod_tarefa:cod_tarefa},
		success:function(){
			
		}
	});
}
function notificar_excluir_integrante(dados){

	var array = dados.split(",");
	var cod_tarefa = array[1].trim();
	var cod_usuario = array[0].trim();
	
	$.ajax({
		url: BASE+'tarefas/notificar_excluir_integrante',
		type:'POST',
		data:{cod_usuario:cod_usuario,cod_tarefa:cod_tarefa},
		success:function(){
			
		}
	});
}
function notificar_exclusao(){

	$.ajax({
		url: BASE+'tarefas/notificar_exclusao',
		type:'POST',
		success:function(){
			
		}
	});
}