<?php
require 'environment.php';

$config = array();
if(ENVIRONMENT == 'development') {
	define("BASE", "http://localhost:8082/ead/");
	define("URL","/ead/");
	define("FOTO","");
	define("ARQUIVO","");
	define("COMPROVANTE","");

	$config['dbname'] = 'somosfilhos';
	$config['host'] = 'localhost';
	$config['dbuser'] = 'root';
	$config['dbpass'] = '';
	
} elseif(ENVIRONMENT == 'production') {

}

global $db;
try {
	$db = new PDO("mysql:dbname=".$config['dbname'].";'charset=utf8',host=".$config['host'], $config['dbuser'], $config['dbpass']);
} catch(PDOException $e) {
	echo "ERRO: ".$e->getMessage();
	exit;
}