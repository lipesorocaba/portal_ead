<?php
session_start();

header("Access-Control-Allow-Origin: *");
header("Access-Control-Allow-Methods: *");
header('Access-Control-Allow-Headers: Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With');
header('Content-Type: text/html; charset=utf-8');

require 'config.php';
require 'vendor/autoload.php';

$core = new Core\Core();
$core->run();